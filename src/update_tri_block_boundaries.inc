! ================================================================================
! Update block boundaries for off-diagonal block loops in self-self interaction
subroutine update_tri_block_boundaries(iblock, offset_n, count_n, &
      istart, iend, jstart, jend)
   use MW_constants, only: pair_block_size
   implicit none
   ! Parameters
   ! ----------
   integer, intent(in) :: iblock     ! block index
   integer, intent(in) :: offset_n   ! offset of particules in data arrays
   integer, intent(in) :: count_n    ! count of particles
   integer, intent(inout) :: istart   ! current start index of block
   integer, intent(inout) :: iend     ! current end index of block
   integer, intent(inout) :: jstart   ! current start index of block
   integer, intent(inout) :: jend     ! current end index of block

   ! Local
   ! -----
   integer :: block_col, block_row

   block_row = floor(0.5*(1.0 + sqrt(1.0 + 8.0*(iblock-1))))+1
   block_col = iblock - ((block_row-1)*(block_row-2))/2

   istart = (block_row-1)*pair_block_size + offset_n + 1
   iend = min(count_n+offset_n, istart + pair_block_size - 1)
   jstart = (block_col-1)*pair_block_size + offset_n + 1
   jend = min(count_n+offset_n, jstart + pair_block_size - 1)

end subroutine update_tri_block_boundaries
