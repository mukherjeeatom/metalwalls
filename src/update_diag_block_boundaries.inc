! ================================================================================
! Update block boundaries for diagonal block loops
subroutine update_diag_block_boundaries(iblock, offset_n, count_n, istart, iend)
   use MW_constants, only: pair_block_size
   implicit none
   ! Parameters
   ! ----------
   integer, intent(in) :: iblock      ! block index
   integer, intent(in) :: offset_n    ! offset in data arrays
   integer, intent(in) :: count_n     ! count of particles
   integer, intent(inout) :: istart   ! current start index of block
   integer, intent(inout) :: iend     ! current end index of block

   istart = (iblock-1)*pair_block_size + offset_n + 1
   iend = min(count_n+offset_n, istart + pair_block_size - 1)

end subroutine update_diag_block_boundaries
