import numpy as np
import mw

# Thermodynamic integration from a charge q1 to a charge q2
# for all ions of type ion_type

# Charge of initial state
q1 = 0
# Charge of final state
q2 = 1

# Ion type (Fortran indexes start at 1)
ion_type = 4

# List of lambda (l) values for the thermodynamic integration
# Defined as U(l) = (1-l) U1 + l U2
lambd_list = np.arange(0, 1.1, 0.1)

# Evaluate energy derivative every num_steps_per_cycle, 
# for num_cycles cycles
num_steps_per_cycle = 10
num_cycles = 100

# Run the thermodynamic integration
mw.thermodynamic_integration.ti_charge_charge(q1, q2, ion_type, lambd_list, num_steps_per_cycle, num_cycles, 'energy_derivative.out')

# Plot the results
mw.thermodynamic_integration.ti_plot('energy_derivative.out', 'ti_charge.png')
