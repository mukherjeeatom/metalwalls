import mw

# List of electrodes belonging to a same group
# using Fortran indices
# 1 = Mg1
# 2 = 01
# 3 = Mg2
# 4 = 02
first_electrode_list = [1,2]
second_electrode_list = [3,4]

# Number of steps of the simulation (optional)
# if none the num_steps defined in the runtime is used
num_steps = 100

# Run the simulation considering the groups defined by 
# first_electrode_list and second_electrode_list as pistons
mw.piston.run_piston(first_electrode_list, second_electrode_list, num_steps)
