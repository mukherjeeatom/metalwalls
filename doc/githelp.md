# Git essential command lines #

## Getting some help ##

`git help <verb>` get the manpage help for the `git <verb>` command.

## Customize your Git environment ##

Use `git config` to customize your Git environment.

### Set up your identity ###

git config --global user.name = "John Doe"
git config --global user.email = "johndoe@example.com"

### Set up your editor ###

git config --global core.editor vi
git config --global core.pager less

## Checking status ##

`git status`    gives output to determine which files are in which states
`git status -s` gives a simplified git status output

`git diff`          shows diff log between local and staged area
`git diff --staged` shows diff log between staged area and last commit

`git log`                                 lists the commits in reverse chronological order (most
     					  recent first)
`git log -p -2`                           lists the last 2 commits with diff following each entry
`git log --stat`                          lists the commits with statistics (list of modified files,
                                          number of lines added/removed in those files) following
					  each entry.
`git log --pretty=format:"%h %s" --graph` lists commits history with abbreviated commit hash (%h)
                                          and commit subject (%s) on one line with a nice little
					  ASCII graph showing branch and merge history

## Committing changes ##

`git commit`                       launches editor to fill commit messages (empty message will abort
                                   commit)
`git commit -m "commit message"`   commits with in-line commit message
`git commit -a`                    add all tracked and modified files to the commit, skipping
                                   staging step
`git commit --amend`               amend previous commit (add newly staged file and modify commit message)

## Removing files ##

`git rm FILE`          stops tracking FILE and removes it from the working tree
`git rm --cached FILE` stops tracking FILE but keeps it in the working tree

# Working with remotes #

`git remote -v` shows remotes shortnames and addresses
`git remote show <name>` shows more information about remote <name>
`git remote add <name> <url>` adds a remote named <name> for the repository at <url>
`git remote rename <oldname> <newname>` rename a remote named <oldname> to <newname>
`git remote remove <name>` removes a remote named <name>

`git fetch <name>` Fetch branches and/or tags from the remote repository named <name>
`git push <repository> <refspec>` Updates remote refs using local refs




