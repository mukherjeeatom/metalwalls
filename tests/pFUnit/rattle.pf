
   ! ========================================================================
   @test
   subroutine test_positions()
      use pfunit_mod
      use MW_kinds, only: wp
      use MW_box, only: MW_box_t, &
            MW_box_define_type => define_type, &
            MW_box_void_type => void_type, &
            MW_box_minimum_image_distance => minimum_image_distance_2DPBC
      use MW_ion, only: MW_ion_t, &
            MW_ion_define_type => define_type, &
            MW_ion_void_type => void_type
      use MW_molecule, only: MW_molecule_t, &
            MW_molecule_define_type => define_type, &
            MW_molecule_void_type => void_type, &
            MW_molecule_set_rattle_parameters => set_rattle_parameters
      use MW_rattle, only: MW_rattle_constrain_positions => constrain_positions
      implicit none
      ! Locals
      ! ------
      integer, parameter :: num_sites = 3
      integer, parameter :: num_constraints = 3

      ! rattle
      integer, parameter :: max_iterations  = 100 !< maximum iteration in rattle
      real(wp) :: tolerance = 1.0e-6 !< tolerance on constraints realisation


      ! Box
      type(MW_box_t) :: box !< simulation box
      real(wp), parameter :: a = 6.09313E+01_wp
      real(wp), parameter :: b = 6.49453E+01_wp
      real(wp), parameter :: c = 2.32893E+02_wp

      ! Time step
      real(wp), parameter :: dt = 8.26820E+01_wp

      ! Molecules
      type(MW_molecule_t) :: molecule

      ! Ions
      type(MW_ion_t) :: ions(num_sites)
      real(wp) :: xyz_now(num_sites,3)
      real(wp) :: velocity_next(num_sites,3)
      real(wp) :: xyz_next(num_sites,3)

      real(wp) :: stress_tensor(3,3)


      real(wp) :: drnorm2, dr
      integer :: ia, ib, iconstraint

      ! Setup
      ! -----
      call MW_box_define_type(box, a, b, c)

      ! Sites
      call MW_ion_define_type(ions(1), "Im1", 1, 0.4373_wp,0.0_wp,0.0_wp,0.0_wp, &
                 0.0_wp, 67.07_wp, .true., .false.)
      ions(1)%offset = 0
      call MW_ion_define_type(ions(2), "Im2", 1, 0.1578_wp,0.0_wp,0.0_wp,0.0_wp, &
                 0.0_wp, 15.04_wp, .true., .false.)
      ions(2)%offset = 1
      call MW_ion_define_type(ions(3), "Im3", 1, 0.1848_wp,0.0_wp,0.0_wp,0.0_wp, &
                 0.0_wp, 57.12_wp, .true., .false.)
      ions(3)%offset = 2

      xyz_now(1,1) = 4.16491E+00_wp
      xyz_now(1,2) = 1.49110E+00_wp
      xyz_now(1,3) = 3.05656E+01_wp

      xyz_now(2,1) = 5.25722E+00_wp
      xyz_now(2,2) = 6.21259E+01_wp
      xyz_now(2,3) = 2.80336E+01_wp

      xyz_now(3,1) = 7.95131E+00_wp
      xyz_now(3,2) = 7.33504E+00_wp
      xyz_now(3,3) = 2.86539E+01_wp


      xyz_next(1,1) = 4.16491E+00_wp
      xyz_next(1,2) = 1.49110E+00_wp
      xyz_next(1,3) = 3.05656E+01_wp

      xyz_next(2,1) = 5.25722E+00_wp
      xyz_next(2,2) = 6.21259E+01_wp
      xyz_next(2,3) = 2.80336E+01_wp

      xyz_next(3,1) = 7.95131E+00_wp
      xyz_next(3,2) = 7.33504E+00_wp
      xyz_next(3,3) = 2.86539E+01_wp

      velocity_next(1,1) =  2.18912E-05_wp
      velocity_next(1,2) =  9.47041E-05_wp
      velocity_next(1,3) = -3.39236E-05_wp

      velocity_next(2,1) =  1.18532E-05_wp
      velocity_next(2,2) =  1.82486E-04_wp
      velocity_next(2,3) = -1.88740E-04_wp

      velocity_next(3,1) =  3.79650E-05_wp
      velocity_next(3,2) =  1.54268E-04_wp
      velocity_next(3,3) =  1.77897E-04_wp

      call MW_molecule_define_type(molecule, "Im41", 1, num_sites, num_constraints, 0, 0, 0, 0, 0, 0, .false., 0.0_wp)
      molecule%sites(1) = 1
      molecule%sites(2) = 2
      molecule%sites(3) = 3

      ! Constraints
      molecule%constrained_sites(1,1) = 1
      molecule%constrained_sites(2,1) = 2
      molecule%constrained_dr(1) = 5.11700E+00_wp

      molecule%constrained_sites(1,2) = 1
      molecule%constrained_sites(2,2) = 3
      molecule%constrained_dr(2) = 7.22100E+00_wp

      molecule%constrained_sites(1,3) = 2
      molecule%constrained_sites(2,3) = 3
      molecule%constrained_dr(3) = 1.052400E+01_wp

      call MW_molecule_set_rattle_parameters(molecule, tolerance, max_iterations)

      ! Test
      ! ----
      call MW_rattle_constrain_positions(2, molecule, dt, box, ions, &
            xyz_now, xyz_next, velocity_next,stress_tensor)

      ! verify that constraints are satisfied
      do iconstraint = 1, num_constraints
         ia = molecule%constrained_sites(1,iconstraint)
         ib = molecule%constrained_sites(2,iconstraint)
         call MW_box_minimum_image_distance(box%length(1), box%length(2), box%length(3), &
               xyz_next(ia,1), xyz_next(ia,2), xyz_next(ia,3), &
               xyz_next(ib,1), xyz_next(ib,2), xyz_next(ib,3), &
               drnorm2)
         dr = sqrt(drnorm2)
         @assertEqual(molecule%constrained_dr(iconstraint), dr, tolerance*molecule%constrained_dr(iconstraint))
      end do

      ! Tear down
      ! ---------
      call MW_box_void_type(box)
      call MW_molecule_void_type(molecule)
      call MW_ion_void_type(ions(1))
      call MW_ion_void_type(ions(2))
      call MW_ion_void_type(ions(3))

   end subroutine test_positions

   ! ========================================================================
   @test
   subroutine test_velocities()
      use pfunit_mod
      use MW_kinds, only: wp
      use MW_box, only: MW_box_t, &
            MW_box_define_type => define_type, &
            MW_box_void_type => void_type, &
            MW_box_minimum_image_displacement => minimum_image_displacement_2DPBC
      use MW_ion, only: MW_ion_t, &
            MW_ion_define_type => define_type, &
            MW_ion_void_type => void_type
      use MW_molecule, only: MW_molecule_t, &
            MW_molecule_define_type => define_type, &
            MW_molecule_void_type => void_type, &
            MW_molecule_set_rattle_parameters => set_rattle_parameters
      use MW_rattle, only: MW_rattle_constrain_velocities => constrain_velocities
      implicit none
      ! Locals
      ! ------
      integer, parameter :: num_sites = 3
      integer, parameter :: num_constraints = 3

      ! rattle
      integer, parameter :: max_iterations  = 100 !< maximum iteration in rattle
      real(wp) :: tolerance = 1.0e-6 !< tolerance on constraints realisation


      ! Box
      type(MW_box_t) :: box !< simulation box
      real(wp), parameter :: a = 6.09313E+01_wp
      real(wp), parameter :: b = 6.49453E+01_wp
      real(wp), parameter :: c = 2.32893E+02_wp

      ! Molecules
      type(MW_molecule_t) :: molecule

      ! Ions
      type(MW_ion_t) :: ions(num_sites)
      real(wp) :: xyz_next(num_sites,3)
      real(wp) :: velocity_next(num_sites,3)



      real(wp) :: drnorm2, dr, rij(3), drijdt(3)
      integer :: ia, ib, iconstraint, ixyz

      ! Setup
      ! -----

      call MW_box_define_type(box, a, b, c)
      call MW_molecule_define_type(molecule, "Im41", 1, num_sites, num_constraints, 0, 0, 0, 0, 0, 0, .false., 0.0_wp)
      molecule%sites(1) = 1
      molecule%sites(2) = 2
      molecule%sites(3) = 3

      ! Site #1
      call MW_ion_define_type(ions(1), "Im1", 1, 0.4373_wp,0.0_wp,0.0_wp,0.0_wp, &
                 0.0_wp, 67.07_wp, .true., .false.)
      ions(1)%offset = 0

      xyz_next(1,1) = 4.87191223460780E+01_wp
      xyz_next(1,2) = 5.19108688854099E+00_wp
      xyz_next(1,3) = 2.12766533412769E+02_wp

      velocity_next(1,1) = -1.72681313183861E-05_wp
      velocity_next(1,2) =  9.35131238552442E-06_wp
      velocity_next(1,3) =  1.41176356694223E-04_wp

      ! Site #2
      call MW_ion_define_type(ions(2), "Im2", 1, 0.1578_wp,0.0_wp,0.0_wp,0.0_wp, &
                 0.0_wp, 15.04_wp, .true., .false.)
      ions(2)%offset = 1
      xyz_next(2,1) = 5.15511939422842E+01_wp
      xyz_next(2,2) = 9.83373839550056E-01_wp
      xyz_next(2,3) = 2.13443447938225E+02_wp

      velocity_next(2,1) = -4.89939276385115E-05_wp
      velocity_next(2,2) = -7.68158692919400E-06_wp
      velocity_next(2,3) =  1.53078164395764E-04_wp

      ! Site #3
      call MW_ion_define_type(ions(3), "Im3", 1, 0.1848_wp,0.0_wp,0.0_wp,0.0_wp, &
                 0.0_wp, 57.12_wp, .true., .false.)
      ions(3)%offset = 2
      xyz_next(3,1) = 5.23580913646423E+01_wp
      xyz_next(3,2) = 1.14019936648806E+01_wp
      xyz_next(3,3) = 2.12196109377520E+02_wp

      velocity_next(3,1) =  4.19721699309307E-05_wp
      velocity_next(3,2) = -4.59782488994706E-05_wp
      velocity_next(3,3) = -8.51986873905301E-05_wp

      ! Constraints
      molecule%constrained_sites(1,1) = 1
      molecule%constrained_sites(2,1) = 2
      molecule%constrained_dr(1) = 5.11700E+00_wp

      molecule%constrained_sites(1,2) = 1
      molecule%constrained_sites(2,2) = 3
      molecule%constrained_dr(2) = 7.22100E+00_wp

      molecule%constrained_sites(1,3) = 2
      molecule%constrained_sites(2,3) = 3
      molecule%constrained_dr(3) = 1.05240E+01_wp

      call MW_molecule_set_rattle_parameters(molecule, tolerance, max_iterations)
      ! Test
      ! ----
      call MW_rattle_constrain_velocities(2, molecule, box, ions, &
            xyz_next, velocity_next)

      ! verify that constraints are satisfied
      do iconstraint = 1, num_constraints
         ia = molecule%constrained_sites(1,iconstraint)
         ib = molecule%constrained_sites(2,iconstraint)
         call MW_box_minimum_image_displacement(box%length(1), box%length(2), box%length(3), &
               xyz_next(ia,1), xyz_next(ia,2), xyz_next(ia,3), &
               xyz_next(ib,1), xyz_next(ib,2), xyz_next(ib,3), &
               rij(1), rij(2), rij(3), drnorm2)
         do ixyz = 1, 3
            drijdt(ixyz) = velocity_next(ib,ixyz) - velocity_next(ia,ixyz)
         end do
         dr = dot_product(drijdt, rij)
         @assertEqual(0.0_wp, dr, tolerance)
      end do

      ! Tear down
      ! ---------
      call MW_box_void_type(box)
      call MW_molecule_void_type(molecule)
      call MW_ion_void_type(ions(1))
      call MW_ion_void_type(ions(2))
      call MW_ion_void_type(ions(3))

   end subroutine test_velocities

