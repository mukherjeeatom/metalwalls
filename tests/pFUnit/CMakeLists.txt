  #########################################
  # Build mw_test binary when option is ON
  #########################################
  
  if (BUILD_TEST)
    set ( SRC_TEST box         configuration_line coulomb_lr      coulomb_self
                   ewald       localwork                          coulomb_keq0
                   coulomb_sr  linear_molecule rattle             cg
    )                   
  # if use MPI
  #coulomb 

    if(DEFINED PFUNIT_INSTALL)
      message(STATUS "Manual setup of variable PFUNIT_INSTALL: ${PFUNIT_INSTALL}")
      set(PFUNIT_DIR ${PFUNIT_INSTALL})
    else()
      include(ExternalProject)

      set(ExternalProjectCMakeArgs
        -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
        -DCMAKE_INSTALL_PREFIX=${PROJECT_BINARY_DIR}/external/pfunit
        -DCMAKE_Fortran_COMPILER=${CMAKE_Fortran_COMPILER}
        )
      ExternalProject_Add(pfunit
        DOWNLOAD_COMMAND git submodule update
        DOWNLOAD_DIR ${PROJECT_SOURCE_DIR}
        SOURCE_DIR ${PROJECT_SOURCE_DIR}/external/pfunit
        BINARY_DIR ${PROJECT_BINARY_DIR}/external/pfunit-build
        STAMP_DIR ${PROJECT_BINARY_DIR}/external/pfunit-stamp
        TMP_DIR ${PROJECT_BINARY_DIR}/external/pfunit-tmp
        INSTALL_DIR ${PROJECT_BINARY_DIR}/external
        CMAKE_ARGS ${ExternalProjectCMakeArgs}
        )
      include_directories(${PROJECT_BINARY_DIR}/external/pfunit/mod)
      set(PFUNIT_DIR ${PROJECT_BINARY_DIR}/external/pfunit)
    endif()

    file(MAKE_DIRECTORY ${PROJECT_BINARY_DIR}/generated)

    include_directories(
      ${PROJECT_SOURCE_DIR}/tests/pFUnit
      ${PROJECT_BINARY_DIR}/generated
      ${PROJECT_BINARY_DIR}/src
      ${PFUNIT_DIR}/mod
      )

    set(_test_sources)
    foreach(_test ${SRC_TEST})
      if(DEFINED PFUNIT_INSTALL)
        set(test_dependency ${PROJECT_SOURCE_DIR}/tests/pFUnit/${_test}.pf)
      else()
        set(test_dependency pfunit ${PROJECT_SOURCE_DIR}/tests/pFUnit/${_test}.pf)
      endif()
      add_custom_command(
        OUTPUT ${PROJECT_BINARY_DIR}/generated/${_test}.F90
        COMMAND python ${PFUNIT_DIR}/bin/pFUnitParser.py ${PROJECT_SOURCE_DIR}/tests/pFUnit/${_test}.pf ${PROJECT_BINARY_DIR}/generated/${_test}.F90
        DEPENDS ${test_dependency}
        )
      set(_test_sources ${_test_sources} ${PROJECT_BINARY_DIR}/generated/${_test}.F90)
    endforeach()

    set_source_files_properties(${PFUNIT_DIR}/include/driver.F90 PROPERTIES GENERATED 1)

    configure_file(${PROJECT_SOURCE_DIR}/tests/pFUnit/AlElectrodes_elec2elec.data ${PROJECT_BINARY_DIR}/tests/pFUnit COPYONLY)
    configure_file(${PROJECT_SOURCE_DIR}/tests/pFUnit/AlElectrodes_melt2elec.data ${PROJECT_BINARY_DIR}/tests/pFUnit COPYONLY)
    configure_file(${PROJECT_SOURCE_DIR}/tests/pFUnit/AlElectrodes_melt_forces.data ${PROJECT_BINARY_DIR}/tests/pFUnit COPYONLY)

    add_executable(
      mw_tests
      ${PFUNIT_DIR}/include/driver.F90
      ${_test_sources}
      )

    target_link_libraries(
      mw_tests 
      ${PFUNIT_DIR}/lib/libpfunit.a
      mw_lib
      )

    add_test(NAME mw_all_tests COMMAND mpirun -n 1 ./mw_tests)

  endif ()
       
