import unittest
import os.path
import glob
from shutil import copyfile
import numpy as np

import mwrun

class test_maze(unittest.TestCase):

  @classmethod
  def setUpClass(self):
    self.test_path = "maze"
    self.mw_exec = os.path.abspath(mwrun.glob_mw_exec)
    self.setup = False

  def run_conf(self, confID, nranks, check_dipoles=False, check_electrodes=False, check_matrix=False, matrix_loaded=False):
    path_to_config = os.path.join(self.test_path, confID)
    n = mwrun.mwrun(self.mw_exec, path_to_config)
    self.workdir = n.workdir

    if matrix_loaded:
      copyfile(os.path.join(n.workdir, "maze_matrix.inpt.ref"), os.path.join(n.workdir, "maze_matrix.inpt"))

    n.run_mw(nranks)
    self.setup = True

    ok, msg = n.compare_datafiles("forces.out", "forces.ref", rtol=1.0E-6, atol=0.0)
    self.assertTrue(ok, msg)

    ok, msg = n.compare_datafiles("energies.out", "energies.ref", rtol=1.0E-6, atol=0.0)
    self.assertTrue(ok, msg)

    ok, msg = n.compare_datafiles("energies_breakdown.out", "energies_breakdown.ref", rtol=1.0E-6, atol=0.0)
    self.assertTrue(ok, msg)

    if check_matrix and mwrun.glob_compare_hessian:
      ok, msg = n.compare_matrixfiles("maze_matrix.inpt", "maze_matrix.inpt.ref", rtol=1.0E-5, atol=0.0)
      self.assertTrue(ok, msg)

    if check_dipoles:
        ok, msg = n.compare_datafiles("dipoles.out", "dipoles.ref", rtol=1.0E-6, atol=0.0)
        self.assertTrue(ok, msg)

    if check_electrodes:
        ok, msg = n.compare_datafiles("charges.out", "charges.ref", rtol=1.0E-6, atol=0.0)
        self.assertTrue(ok, msg)

  def tearDown(self):
    if self.setup:
      for f in glob.glob(os.path.join(self.workdir, "*.out")):
        os.remove(f)
      if os.path.isfile(os.path.join(self.workdir, "maze_matrix.inpt")):
        os.remove(os.path.join(self.workdir, "maze_matrix.inpt"))

#  def test_conf_2D_ishii_nodisp_iter(self):
#    self.run_conf("dipoles/2D-ishii-nodisp-iter", 1, check_dipoles=True)
#
#  def test_conf_2D_ishii_nodisp_iter_4MPI(self):
#    self.run_conf("dipoles/2D-ishii-nodisp-iter", 4, check_dipoles=True)
#
#  def test_conf_2D_ishii_nodisp_invert(self):
#    if mwrun.glob_skip_long_test:
#      self.skipTest("Test too long")
#    self.run_conf("dipoles/2D-ishii-nodisp-invert", 1, check_dipoles=True)
#
#  def test_conf_2D_ishii_nodisp_invert_4MPI(self):
#    if mwrun.glob_skip_long_test:
#      self.skipTest("Test too long")
#    self.run_conf("dipoles/2D-ishii-nodisp-invert", 4, check_dipoles=True)

  def test_conf_pol3_iter(self):
    self.run_conf("dipoles/pol3-iter", 1, check_dipoles=True)

  def test_conf_pol3_iter_4MPI(self):
    self.run_conf("dipoles/pol3-iter", 4, check_dipoles=True)

  def test_conf_pol3_invert(self):
    if mwrun.glob_skip_long_test:
      self.skipTest("Test too long")
    self.run_conf("dipoles/pol3-invert", 1, check_dipoles=True)

  def test_conf_pol3_invert_4MPI(self):
    if mwrun.glob_skip_long_test:
      self.skipTest("Test too long")
    self.run_conf("dipoles/pol3-invert", 4, check_dipoles=True)

  def test_conf_LiClwAl_2blocks(self):
     self.run_conf("electrodes/LiCl-Al-2blocks", 1, check_electrodes=True)

  def test_conf_LiClwAl_2blocks_4MPI(self):
     self.run_conf("electrodes/LiCl-Al-2blocks", 4, check_electrodes=True)

  # def test_conf_LiClwAl_CG_store(self):
  #    self.run_conf("electrodes/LiCl-Al-CG-store", 1, check_matrix=True, check_electrodes=True)
  #
  # def test_conf_LiClwAl_CG_store_4MPI(self):
  #   self.run_conf("electrodes/LiCl-Al-CG-store", 4, check_matrix=True, check_electrodes=True)
  #
  # def test_conf_LiClwAl_CG_store_matrix_loaded(self):
  #    self.run_conf("electrodes/LiCl-Al-CG-store", 1, matrix_loaded=True, check_electrodes=True)

  # def test_conf_LiClwAl_CG_nostore(self):
  #    self.run_conf("electrodes/LiCl-Al-CG-nostore", 1, check_electrodes=True)
  #
  # def test_conf_LiClwAl_CG_nostore_4MPI(self):
  #   self.run_conf("electrodes/LiCl-Al-CG-nostore", 4, check_electrodes=True)

  def test_conf_LiClwAldpsi1_CG_nostore(self):
    self.run_conf("electrodes/LiCl-Al-dpsi1-CG-nostore", 1, check_electrodes=True)

  def test_conf_LiClwAldpsi1_CG_nostore_4MPI(self):
    self.run_conf("electrodes/LiCl-Al-dpsi1-CG-nostore", 4, check_electrodes=True)

  def test_conf_LiClwAldpsi1_invert(self):
     self.run_conf("electrodes/LiCl-Al-dpsi1", 1, check_matrix=True, check_electrodes=True)

  def test_conf_LiClwAldpsi1_invert_4MPI(self):
    self.run_conf("electrodes/LiCl-Al-dpsi1", 4, check_matrix=True, check_electrodes=True)

  def test_conf_LiClwAldpsi1_invert_matrix_loaded(self):
     self.run_conf("electrodes/LiCl-Al-dpsi1", 1, matrix_loaded=True, check_electrodes=True)

  # def test_conf_LiClwAl_invert(self):
  #    self.run_conf("electrodes/LiCl-Al", 1, check_matrix=True, check_electrodes=True)
  #
  # def test_conf_LiClwAl_invert_4MPI(self):
  #   self.run_conf("electrodes/LiCl-Al", 4, check_matrix=True, check_electrodes=True)
  #
  # def test_conf_LiClwAl_invert_matrix_loaded(self):
  #    self.run_conf("electrodes/LiCl-Al", 1, matrix_loaded=True, check_electrodes=True)

  # def test_conf_LiClwAl_invert_charge_neutrality(self):
  #    self.run_conf("electrodes/LiCl-Al-charge-neutrality", 1, check_matrix=True, check_electrodes=True)
  #
  # def test_conf_LiClwAl_invert_charge_neutrality_4MPI(self):
  #   self.run_conf("electrodes/LiCl-Al-charge-neutrality", 4, check_matrix=True, check_electrodes=True)
  #
  # def test_conf_LiClwAl_invert_charge_neutrality_matrix_loaded(self):
  #    self.run_conf("electrodes/LiCl-Al-charge-neutrality", 1, matrix_loaded=True, check_electrodes=True)

  def test_conf_LiClwAl_invert_charge_neutrality_dpsi1(self):
     self.run_conf("electrodes/LiCl-Al-charge-neutrality-dpsi1", 1, check_matrix=True, check_electrodes=True)

  def test_conf_LiClwAl_invert_charge_neutrality_dpsi1_4MPI(self):
    self.run_conf("electrodes/LiCl-Al-charge-neutrality-dpsi1", 4, check_matrix=True, check_electrodes=True)

  def test_conf_LiClwAl_invert_charge_neutrality_dpsi1_matrix_loaded(self):
     self.run_conf("electrodes/LiCl-Al-charge-neutrality-dpsi1", 1, matrix_loaded=True, check_electrodes=True)

  # def test_conf_supercap(self):
  #    self.run_conf("electrodes/supercap", 1)

  # def test_conf_supercap_4MPI(self):
  #   self.run_conf("electrodes/supercap", 4)
