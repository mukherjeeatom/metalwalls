import unittest
import os.path
import glob
import numpy as np
import os

import mwrun

class test_unwrap(unittest.TestCase):

  @classmethod
  def setUpClass(self):
    self.test_path = "unwrap"
    self.mw_exec = os.path.abspath(mwrun.glob_mw_exec)

  def run_conf(self, confID, nranks):
    path_to_config = os.path.join(self.test_path, confID)
    n = mwrun.mwrun(self.mw_exec, path_to_config)
    self.workdir = n.workdir
    n.run_mw(nranks)
	
    lammps_ref = os.path.join(self.workdir, "lammpstrj.ref")
    lammps_trj = os.path.join(self.workdir, "trajectories.lammpstrj")
    ok, msg = n.diff_files(lammps_ref, lammps_trj)
    self.assertTrue(ok, msg)
    

  def tearDown(self):
    for f in glob.glob(os.path.join(self.workdir, "*.out")):
      os.remove(f)
    os.remove(os.path.join(self.workdir, "trajectories.lammpstrj"))

  def test_conf_unwrap_npt(self):
    self.run_conf("npt", 1)

  def test_conf_unwrap_npt_4MPI(self):
    self.run_conf("npt", 4)

  def test_conf_unwrap_nvt(self):
    self.run_conf("nvt", 1)

  def test_conf_unwrap_nvt_4MPI(self):
    self.run_conf("nvt", 4)

