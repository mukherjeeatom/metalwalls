# System configuration
# ====================

# Global simulation parameters
# ----------------------------
num_steps       200 # number of steps in the run
timestep     82.682 # timestep (a.u)
temperature  1800.0 # temperature (K)

# Periodic boundary conditions
# ----------------------------
num_pbc  3

# Thermostat:
# -----------
thermostat
  chain_length  5
  relaxation_time 4134.1
  tolerance  1.0e-17
  max_iteration  100

barostat
  pressure 8.899737356e-7
  chain_length 5
  relaxation_time 20670.5

# Species definition
# ----------------------
species

  species_type
    name   Cl              # name of the species
    count  256             # number of species 
    mass   35.453          # mass in amu
    mobile True
    charge point -1.000    # permanent charge on the ions

  species_type
    name   Na              # name of the species
    count  256             # number of species 
    mass   22.990          # mass in amu
    mobile True
    charge point 1.000     # permanent charge on the ions

# Interactions definition
# -----------------------
interactions
  coulomb
    coulomb_rtol 1.63e-5   # coulomb rtol
    coulomb_rcut 23.5      # coulomb cutoff (bohr)
    coulomb_ktol 1.0e-7    # coulomb ktol

  fumi-tosi
    ft_rcut  23.5          # ft cutoff (bohr)
    ft_3D_pressure_tail_correction
    ft_pair   Cl   Cl  1.797 275.1 140.0 280.0 1.7 1.7
    ft_pair   Cl   Na  1.726  67.5  47.4 187.3 1.7 1.7
    ft_pair   Na   Na  1.000   0.0  11.7  51.8 1.7 1.7

# Output section
# --------------
output
  default 0
  step 50
  lammps 10 unwrap
