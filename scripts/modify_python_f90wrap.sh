# Ad hoc modifications
# Rename subroutines that have the same name in different modules
# Rename short_bn and long_bn to short and long for command otherwise the names do not correspond to the .o files... not sure why this happens
for module in `ls -1 src/*f90 src/*F90 | sed -e 's/\.f90//' | sed -e 's/\.F90//' | sed -e 's/src\///'`
do
   echo "Modifying module $module"
   sed -i -e "s/subroutine f90wrap_void_type/subroutine f90wrap_${module}_void_type/g" f90wrap_${module}.f90 
   sed -i -e "s/subroutine f90wrap_define_type/subroutine f90wrap_${module}_define_type/g" f90wrap_${module}.f90       
   sed -i -e "s/subroutine f90wrap_copy_type/subroutine f90wrap_${module}_copy_type/g" f90wrap_${module}.f90           
   sed -i -e "s/subroutine f90wrap_print_type/subroutine f90wrap_${module}_print_type/g" f90wrap_${module}.f90         
   sed -i -e "s/subroutine f90wrap_initialize/subroutine f90wrap_${module}_initialize/g" f90wrap_${module}.f90         
   sed -i -e "s/subroutine f90wrap_compute/subroutine f90wrap_${module}_compute/g" f90wrap_${module}.f90         
   sed -i -e "s/subroutine f90wrap_finalize/subroutine f90wrap_${module}_finalize/g" f90wrap_${module}.f90             
   sed -i -e "s/subroutine f90wrap_set_rcut/subroutine f90wrap_${module}_set_rcut/g" f90wrap_${module}.f90             
   sed -i -e "s/subroutine f90wrap_set_parameters/subroutine f90wrap_${module}_set_parameters/g" f90wrap_${module}.f90
   sed -i -e "s/subroutine f90wrap_set_force_neutral/subroutine f90wrap_${module}_set_force_neutral/g" f90wrap_${module}.f90
   sed -i -e "s/subroutine f90wrap_set_max_iterations/subroutine f90wrap_${module}_set_max_iterations/g" f90wrap_${module}.f90
   sed -i -e "s/subroutine f90wrap_set_tolerance/subroutine f90wrap_${module}_set_tolerance/g" f90wrap_${module}.f90
   sed -i -e "s/subroutine f90wrap_setup_history/subroutine f90wrap_${module}_setup_history/g" f90wrap_${module}.f90
   sed -i -e "s/subroutine f90wrap_update_history/subroutine f90wrap_${module}_update_history/g" f90wrap_${module}.f90
   sed -i -e "s/subroutine f90wrap_print_statistics/subroutine f90wrap_${module}_print_statistics/g" f90wrap_${module}.f90
   sed -i -e "s/subroutine f90wrap_reset_statistics/subroutine f90wrap_${module}_reset_statistics/g" f90wrap_${module}.f90
   sed -i -e "s/subroutine f90wrap_allocate_arrays/subroutine f90wrap_${module}_allocate_arrays/g" f90wrap_${module}.f90
   sed -i -e "s/subroutine f90wrap_energy/subroutine f90wrap_${module}_energy/g" f90wrap_${module}.f90
   sed -i -e "s/subroutine f90wrap_set_index/subroutine f90wrap_${module}_set_index/g" f90wrap_${module}.f90
   sed -i -e "s/subroutine f90wrap_read_parameters/subroutine f90wrap_${module}_read_parameters/g" f90wrap_${module}.f90
   sed -i -e "s/subroutine f90wrap_set_write_matrix/subroutine f90wrap_${module}_set_write_matrix/g" f90wrap_${module}.f90
   sed -i -e "s/subroutine f90wrap_set_charge_neutrality/subroutine f90wrap_${module}_set_charge_neutrality/g" f90wrap_${module}.f90
   sed -i -e "s/subroutine f90wrap_mixing_rule/subroutine f90wrap_${module}_mixing_rule/g" f90wrap_${module}.f90
   sed -i -e "s/subroutine f90wrap_allocate_velocity_work_array/subroutine f90wrap_${module}_allocate_velocity_work_array/g" f90wrap_${module}.f90
   sed -i -e "s/subroutine f90wrap_setup_masses/subroutine f90wrap_${module}_setup_masses/g" f90wrap_${module}.f90
#   sed -i -e "s/short_bn/short/g" f90wrap_${module}.f90
#   sed -i -e "s/short_bn/short/g" metalwalls.py
#   sed -i -e "s/long_bn/long/g" f90wrap_${module}.f90
#   sed -i -e "s/long_bn/long/g" metalwalls.py
#   sed -i -e "s/rank_bn/rank/g" f90wrap_${module}.f90
#   sed -i -e "s/rank_bn/rank/g" metalwalls.py
#   sed -i -e "s/size_bn/size/g" f90wrap_${module}.f90
#   sed -i -e "s/size_bn/size/g" metalwalls.py
   sed -i -e "s/character(1024), intent(in), dimension(n0) :: species_names/character(8), intent(in), dimension(n0) :: species_names/g" f90wrap_${module}.f90
   sed -i -e "s/character(1024), intent(in), dimension(n0) :: names/character(8), intent(in), dimension(n0) :: names/g" f90wrap_${module}.f90
   sed -i -e "s/character(1024), intent(in), dimension(n0) :: energies_to_dump/character(128), intent(in), dimension(n0) :: energies_to_dump/g" f90wrap_${module}.f90
   class=`echo -e $module | sed -r "s/_/ /g;s/\<./\U&/g;s/ /_/g"` 
   sed -i -e "/^class Mw_${class}(/,/^mw_${module} = Mw_${class}()/ s/_metalwalls.f90wrap_void_type/_metalwalls.f90wrap_${module}_void_type/" metalwalls.py           
   sed -i -e "/^class Mw_${class}(/,/^mw_${module} = Mw_${class}()/ s/_metalwalls.f90wrap_define_type/_metalwalls.f90wrap_${module}_define_type/" metalwalls.py       
   sed -i -e "/^class Mw_${class}(/,/^mw_${module} = Mw_${class}()/ s/_metalwalls.f90wrap_copy_type/_metalwalls.f90wrap_${module}_copy_type/" metalwalls.py           
   sed -i -e "/^class Mw_${class}(/,/^mw_${module} = Mw_${class}()/ s/_metalwalls.f90wrap_print_type/_metalwalls.f90wrap_${module}_print_type/" metalwalls.py         
   sed -i -e "/^class Mw_${class}(/,/^mw_${module} = Mw_${class}()/ s/_metalwalls.f90wrap_initialize/_metalwalls.f90wrap_${module}_initialize/" metalwalls.py         
   sed -i -e "/^class Mw_${class}(/,/^mw_${module} = Mw_${class}()/ s/_metalwalls.f90wrap_compute/_metalwalls.f90wrap_${module}_compute/" metalwalls.py         
   sed -i -e "/^class Mw_${class}(/,/^mw_${module} = Mw_${class}()/ s/_metalwalls.f90wrap_finalize/_metalwalls.f90wrap_${module}_finalize/" metalwalls.py             
   sed -i -e "/^class Mw_${class}(/,/^mw_${module} = Mw_${class}()/ s/_metalwalls.f90wrap_set_rcut/_metalwalls.f90wrap_${module}_set_rcut/" metalwalls.py             
   sed -i -e "/^class Mw_${class}(/,/^mw_${module} = Mw_${class}()/ s/_metalwalls.f90wrap_set_parameters/_metalwalls.f90wrap_${module}_set_parameters/" metalwalls.py
   sed -i -e "/^class Mw_${class}(/,/^mw_${module} = Mw_${class}()/ s/_metalwalls.f90wrap_set_force_neutral/_metalwalls.f90wrap_${module}_set_force_neutral/" metalwalls.py
   sed -i -e "/^class Mw_${class}(/,/^mw_${module} = Mw_${class}()/ s/_metalwalls.f90wrap_set_max_iterations/_metalwalls.f90wrap_${module}_set_max_iterations/" metalwalls.py
   sed -i -e "/^class Mw_${class}(/,/^mw_${module} = Mw_${class}()/ s/_metalwalls.f90wrap_set_tolerance/_metalwalls.f90wrap_${module}_set_tolerance/" metalwalls.py
   sed -i -e "/^class Mw_${class}(/,/^mw_${module} = Mw_${class}()/ s/_metalwalls.f90wrap_setup_history/_metalwalls.f90wrap_${module}_setup_history/" metalwalls.py
   sed -i -e "/^class Mw_${class}(/,/^mw_${module} = Mw_${class}()/ s/_metalwalls.f90wrap_update_history/_metalwalls.f90wrap_${module}_update_history/" metalwalls.py
   sed -i -e "/^class Mw_${class}(/,/^mw_${module} = Mw_${class}()/ s/_metalwalls.f90wrap_print_statistics/_metalwalls.f90wrap_${module}_print_statistics/" metalwalls.py
   sed -i -e "/^class Mw_${class}(/,/^mw_${module} = Mw_${class}()/ s/_metalwalls.f90wrap_reset_statistics/_metalwalls.f90wrap_${module}_reset_statistics/" metalwalls.py
   sed -i -e "/^class Mw_${class}(/,/^mw_${module} = Mw_${class}()/ s/_metalwalls.f90wrap_allocate_arrays/_metalwalls.f90wrap_${module}_allocate_arrays/" metalwalls.py
   sed -i -e "/^class Mw_${class}(/,/^mw_${module} = Mw_${class}()/ s/_metalwalls.f90wrap_energy/_metalwalls.f90wrap_${module}_energy/" metalwalls.py
   sed -i -e "/^class Mw_${class}(/,/^mw_${module} = Mw_${class}()/ s/_metalwalls.f90wrap_set_index/_metalwalls.f90wrap_${module}_set_index/" metalwalls.py
   sed -i -e "/^class Mw_${class}(/,/^mw_${module} = Mw_${class}()/ s/_metalwalls.f90wrap_read_parameters/_metalwalls.f90wrap_${module}_read_parameters/" metalwalls.py
   sed -i -e "/^class Mw_${class}(/,/^mw_${module} = Mw_${class}()/ s/_metalwalls.f90wrap_set_write_matrix/_metalwalls.f90wrap_${module}_set_write_matrix/" metalwalls.py
   sed -i -e "/^class Mw_${class}(/,/^mw_${module} = Mw_${class}()/ s/_metalwalls.f90wrap_set_charge_neutrality/_metalwalls.f90wrap_${module}_set_charge_neutrality/" metalwalls.py
   sed -i -e "/^class Mw_${class}(/,/^mw_${module} = Mw_${class}()/ s/_metalwalls.f90wrap_mixing_rule/_metalwalls.f90wrap_${module}_mixing_rule/" metalwalls.py
   sed -i -e "/^class Mw_${class}(/,/^mw_${module} = Mw_${class}()/ s/_metalwalls.f90wrap_allocate_velocity_work_array/_metalwalls.f90wrap_${module}_allocate_velocity_work_array/" metalwalls.py
   sed -i -e "/^class Mw_${class}(/,/^mw_${module} = Mw_${class}()/ s/_metalwalls.f90wrap_setup_masses/_metalwalls.f90wrap_${module}_setup_masses/" metalwalls.py
done
# Specific corrections due to preprocessing 
#sed -i '0,/end subroutine f90wrap_mw_constants__get__cache_line_size/! {/f90wrap_mw_constants__get__cache_line_size/,/f90wrap_mw_constants__get__cache_line_size/d}' f90wrap_constants.f90
#sed -i '0,/end subroutine f90wrap_mw_constants__get__pair_block_size/! {/f90wrap_mw_constants__get__pair_block_size/,/f90wrap_mw_constants__get__pair_block_size/d}' f90wrap_constants.f90
#sed -i '0,/end subroutine f90wrap_mw_constants__get__block_vector_size/! {/f90wrap_mw_constants__get__block_vector_size/,/f90wrap_mw_constants__get__block_vector_size/d}' f90wrap_constants.f90
#sed -i '0,/subroutine \&/! s/subroutine \&//' f90wrap_constants.f90
# Modify f90wrap_kinds.f90 because of preprocessor flags
#sed -i '/f90wrap_kinds_inquire_real16(/i #ifdef MW_use_real16' f90wrap_kinds.f90
#sed -i '/f90wrap_kinds_inquire_real16$/a #endif' f90wrap_kinds.f90
#sed -i '/f90wrap_kinds_inquire_qd(/i #ifdef MW_use_real16' f90wrap_kinds.f90
#sed -i '/f90wrap_kinds_inquire_qd$/a #endif' f90wrap_kinds.f90
#sed -i '/f90wrap_kinds_inquire_real10(/i #ifdef MW_use_real10' f90wrap_kinds.f90
#sed -i '/f90wrap_kinds_inquire_real10$/a #endif' f90wrap_kinds.f90
#sed -i '/f90wrap_kinds_inquire_ep(/i #ifdef MW_use_real10' f90wrap_kinds.f90
#sed -i '/f90wrap_kinds_inquire_ep$/a #endif' f90wrap_kinds.f90
#sed -i '/f90wrap_kinds_inquire_int16(/i #ifdef MW_use_INT16' f90wrap_kinds.f90
#sed -i '/f90wrap_kinds_inquire_int16$/a #endif' f90wrap_kinds.f90
#sed -i '/f90wrap_mw_kinds__get__real16(/i #ifdef MW_use_real16' f90wrap_kinds.f90
#sed -i '/f90wrap_mw_kinds__get__real16$/a #endif' f90wrap_kinds.f90
#sed -i '/f90wrap_mw_kinds__get__qd(/i #ifdef MW_use_real16' f90wrap_kinds.f90
#sed -i '/f90wrap_mw_kinds__get__qd$/a #endif' f90wrap_kinds.f90
#sed -i '/f90wrap_mw_kinds__get__real10(/i #ifdef MW_use_real10' f90wrap_kinds.f90
#sed -i '/f90wrap_mw_kinds__get__real10$/a #endif' f90wrap_kinds.f90
#sed -i '/f90wrap_mw_kinds__get__ep(/i #ifdef MW_use_real10' f90wrap_kinds.f90
#sed -i '/f90wrap_mw_kinds__get__ep$/a #endif' f90wrap_kinds.f90
#sed -i '/f90wrap_mw_kinds__get__int16(/i #ifdef MW_use_INT16' f90wrap_kinds.f90
#sed -i '/f90wrap_mw_kinds__get__int16$/a #endif' f90wrap_kinds.f90
